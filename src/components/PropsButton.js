import styled from "styled-components"

const PropsButton = styled.button`
background-color: ${props => props.backColor || "red"};
color:  ${props => props.fontColor || "yellow"};
border: none;
padding: 10px 20px;
border-radius: 5px;
margin: 10px;
font-size: 14px;
`

const PrimaryButton = styled.button`
background-color: ${props => props.primary ? "#7b4cd8" : "#ff31ca"};
color:  ${props => props.primary ? "#e0d9f3" : "#ffb3e6"};
border: none;
padding: 10px 20px;
border-radius: 5px;
margin: 10px;
font-size: 14px;
`

export {PropsButton, PrimaryButton}