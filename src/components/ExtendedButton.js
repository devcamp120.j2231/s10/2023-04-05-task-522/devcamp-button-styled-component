import styled from "styled-components"

const SuperButton = styled.button`
border: none;
padding: 10px 20px;
border-radius: 10px;
margin: 20px;
font-size: 16px;
font-weight:bold;
`

const ExtendedButton = styled(SuperButton)`
background-color: ${props => props.bgColor || "#ff7f50"};
color:  ${props => props.ftColor || "white"};
`

export {SuperButton, ExtendedButton }